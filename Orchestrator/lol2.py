
import pika
import json
import threading 
import time


class masterSlave(object):
    def __init__(self):
        self.type = "slave"
        self.thread = threading.Thread(target=self.slave_target, args=())
        

    def slave_target(self):
        connection = pika.BlockingConnection(
        pika.ConnectionParameters(host='localhost'))
        channel = connection.channel()

        channel.queue_declare(queue='hello')
            
        while(self.type=="slave"):
            a = channel.basic_get(queue="hello")
            #channel.stop_consuming()
            if(a[0]!=None):
                print(a[2], '  YES MASTER')


    def master_target(self):
        connection = pika.BlockingConnection(
        pika.ConnectionParameters(host='localhost'))
        channel = connection.channel()

        channel.queue_declare(queue='hello')
            
        while(self.type=="master"):
            a = channel.basic_get(queue="hello")
            #channel.stop_consuming()
            if(a[0]!=None):
                print(a[2], '  ONE AT A TIME')
    
    def slave_to_master(self):
        print("\n\n\nSuccesfully changed to master\n\n\n")
        self.type = "master"
        self.thread = threading.Thread(target=self.master_target, args=())
        self.run()




    def run(self):
        self.thread.start()




def callback(ch, method, properties, body):
    print(" [x] Received %r" % body)



def stopConsuming(channel):
    time.sleep(5)
    channel.stop_consuming()



print(' [*] Waiting for messages. To exit press CTRL+C')
#channel.start_consuming()
connection = pika.BlockingConnection(
pika.ConnectionParameters(host='localhost'))
channel = connection.channel()
lol = masterSlave()

lol.run()
time.sleep(5)
lol.slave_to_master()

