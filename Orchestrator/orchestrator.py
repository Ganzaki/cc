import pika
import re
import json
import requests
import pandas as pd
from datetime import datetime
from flask import Flask, request, jsonify, abort, Response
import docker
import threading
from kazoo.client import KazooClient
from kazoo.client import KazooState
import logging
import os
import signal
import time
#import subprocess

app = Flask(__name__)

############################# RABBITMQ QUEUE DECLERATION #############################
connection = pika.BlockingConnection(
        pika.ConnectionParameters(host='rmq', heartbeat=0))
channel = connection.channel()

channel.queue_declare(queue='writeQ')
channel.queue_declare(queue='readQ')


channel.queue_declare(queue = "lol")
######################################################################################


logging.basicConfig()
zk = KazooClient(hosts='zoo:2181')
zk.start()

zk.ensure_path("/slave")
zk.ensure_path("/master")
lock1 = zk.Lock("/slave", "l1")
lock2 = zk.Lock("/master", "l2")

net_mode = "orchestrator_default"
path = "/root/Documents/Cloud/Project/Project/cc/Orchestrator/"
vol = {path:{"bind":"/mnt/code", "mode":"rw"}}
links = {"rmq":"rmq", "zoo":"zoo"}
client = docker.DockerClient(base_url='tcp://172.31.82.57:2375')
sssss = 1

firstElection = 3
current_master = None
masterPresent = False

globalLock = True
globalreadLock = True

responseDict = {}
responseWDict = {}



def getResponseW():
    global responseWDict
    connection2 = pika.BlockingConnection(
        pika.ConnectionParameters(host='rmq'))
    channel2 = connection2.channel()

    channel2.queue_declare(queue = "responsewQ")
    while(True):
        res = channel2.basic_get(queue="responsewQ")

        # dequeque from `responsewQ` until response is got 
        while(res[2]==None):
            res = channel2.basic_get(queue="responsewQ")

        data = json.loads(res[2])
        print(data)
        reqID = int(list(data.keys())[0])
        
        responseWDict[reqID] = data[str(reqID)]
        fff = open("rw.txt", 'w')
        fff.write(json.dumps(responseWDict))
        fff.close()

def getResponse():
    global responseDict
    connection3 = pika.BlockingConnection(
        pika.ConnectionParameters(host='rmq'))
    channel3 = connection3.channel()
    channel3.queue_declare(queue = "responseQ")
    while(True):
        res = channel3.basic_get(queue="responseQ")

        # dequeque from `responseQ` until response is got 
        while(res[2]==None):
            res = channel3.basic_get(queue="responseQ")

        data = json.loads(res[2])
        print(data)
        reqID = int(list(data.keys())[0])
        responseDict[reqID] = data[str(reqID)]
        fff = open("wr.txt", 'w')
        fff.write(json.dumps(data))





def downScale():
    global globalreadLock
    while(True):
        time.sleep(0.01)
        ttt = open("timer.txt", "r")
        tt = float(ttt.readline())
        while(time.time()-tt<120):
            pass
            # if (time.time()-tt >= 55 and time.time()-tt<=56) or (time.time()-tt >= 110 and time.time()-tt<=111):

            #     # channel.basic_publish(exchange='', routing_key='lol', body='testing')
            #     # tmp = channel.basic_get(queue='lol')
        slaves_pid = getOnlyInt(zk.get_children("/slave"))
        while(globalreadLock == False):
            pass
        globalreadLock = False
        fff = open("counter.txt", "r")
        count = int(fff.readline())
        cc = len(slaves_pid) - (int((count-1) / 20) + 1)
        while(cc>0):
            requests.get(url = 'http://0.0.0.0:8080/api/v1/crash/slave')
            cc = cc - 1
        fff.close()
        fff = open("counter.txt", "w")
        fff.write(str(0))
        fff.close()
        globalreadLock = True
        ttt.close()
        ttt = open("timer.txt", "w")
        ttt.write(str(time.time()))
        ttt.close()



def ReadIsToSlave():
    global globalreadLock
    while(globalreadLock == False):
        pass
    globalreadLock = False
    fff = open("counter.txt", "r")
    count = int(fff.readline())
    count = count + 1
    fff.close()
    fff = open("counter.txt", "w")
    fff.write(str(count))
    slaves_pid = getOnlyInt(zk.get_children("/slave"))
    cc = int((count-1) / 20) + 1 - len(slaves_pid)
    while(cc>0):
        print("\n\n\n\n\n\n Count = ", cc, "\n\n\n\n\n\n" )
        slavePatrols(1)
        time.sleep(3)
        cc = cc - 1
    globalreadLock = True

def getOnlyInt(arr):
    a = []
    for i in arr:
        try:
            a.append(int(i))
        except:
            pass
    return a


def firstElectionTimer():
    global firstElection
    time.sleep(3)
    firstElection = 0


def slavePatrols(ev):
    # print("\n\n---------------------------lol----------------------------\n\n")
    # print("\n\n---------------------------slave----------------------------\n\n")
    # print("\n\n---------------------------lol----------------------------\n\n")
    global masterPresent
    global current_master
    global firstElection
    firstElection = 3
    
    slave = client.containers.run("slave:latest", network_mode="orchestrator_default", links=links, command="python3 slave.py", detach = True)
    time.sleep(2)
    firstElection = 3
    timer = threading.Thread(target=firstElectionTimer, args=())
    timer.start()
    if(masterPresent==True):
        _,bits = current_master.exec_run(["cat", "users.csv"])
        f = open("users.csv", "wb")
        f.write(bits)
        f.close()
        f = open("users.csv", "rb")
        bits = f.read()
        f.close()
        slave.exec_run(["rm", "/usr/src/app/users.csv"])
        slave.put_archive('/usr/src/app/users.csv', bits)
        bits, stat = current_master.get_archive('/usr/src/app/rides.csv')
        slave.exec_run(["rm", "/usr/src/app/rides.csv"])
        slave.put_archive('/usr/src/app/rides.csv', bits)
        try:
            current_master.export()
        except Exception as e:
            print(e)
        time.sleep(0.5)
    return slave.id
    
    

def masterElection(ev):
    # print("\n\n---------------------------lol----------------------------\n\n")
    # print("\n\n---------------------------master----------------------------\n\n")
    # print("\n\n---------------------------lol----------------------------\n\n")
    # print("Master ", ev, "   ", os.uname())
    # containers = client.containers.list()
    global globalreadLock
    global masterPresent
    global current_master
    masterPresent = True
    slaves = dict()
    masters = getOnlyInt(zk.get_children("/master"))
    # slaves = getOnlyInt(zk.get_children("/slave"))
    # while(len(slaves)<1):
    #     slaves = getOnlyInt(zk.get_children("/slave"))
    if(len(masters) == 0):
        while(globalreadLock == False):
            pass
        globalreadLock = False
        global firstElection
        time.sleep(firstElection)
        slaves_pid = getOnlyInt(zk.get_children("/slave", watch=slavePatrols))
        for slave in slaves_pid:
            print(zk.get("/slave/"+str(slave)))
            slaves[slave] = zk.get("/slave/"+str(slave))[0].decode()
        slaves_pid = sorted(slaves_pid)
        time.sleep(firstElection)
        cont_id = zk.get("/slave/"+str(slaves_pid[0]))[0].decode()
        
        slave = client.containers.get(cont_id)
        time.sleep(firstElection)
        zzz2=slave.exec_run(["cat","slave_master.txt"])
        sss=slave.exec_run(["kill","1"])
        zzz=slave.exec_run(["cat","slave_master.txt"])
        current_master = slave
        global sssss
        lll = open("fucker"+str(sssss)+".txt", 'w')
        sssss = sssss + 1
        sss = [str(s) for s in sss]
        zzz = [str(z) for z in zzz]
        zzz2 = [str(z) for z in zzz2]
        lll.write(" ".join(sss)+'\n'+" ".join(zzz2)+"\n"+" ".join(zzz)+"\n"+cont_id+"\n")
        lll.close()
        time.sleep(2)
        #zk.get_children("/master", watch=masterElection)
        # with lock5, lock6:
        globalreadLock = True
        return slave.id
    return 0
    



def doNothing(event):
    pass




@app.route('/api/v1/db/write', methods=['POST'])
def db_write():
    '''
        write request is taken and
        passed to writeQ
    '''

    fff = open("WreqID.txt", 'r')
    reqID = int(fff.readline()) + 1
    fff.close()
    fff = open("WreqID.txt", 'w')
    fff.write(str(reqID))
    fff.close()
    # print("\n\n---------------------------lol----------------------------\n\n")
    # data from `post` request
    data = request.get_json()

    body = {}
    body[reqID] = data

    # publish the data onto the `writeQ` queue
    channel.basic_publish(exchange='', routing_key='writeQ', body=json.dumps(body))
    global masterPresent
    masterPresent = True

    # getting response from the `responseQ` queue
    

    # dequeque from `responseQ` until response is got 
    print(reqID)
    global responseWDict
    while (reqID not in list(responseWDict.keys())):
        fff = open("rrr.txt", 'w')
        fff.write(json.dumps(responseWDict))
        fff.close()

    res = responseWDict[reqID]
    responseWDict.pop(reqID)
    # response in the form {result:`result`, status:`status-code`}
    # print(res)
    # print("\n\n---------------------------lol----------------------------\n\n")

    return jsonify(json.loads(res["result"])), res["status"]



@app.route('/api/v1/db/read', methods=['POST'])
def db_read():
    '''
        read request is taken and
        passed to readQ
    '''

    fff = open("RreqID.txt", 'r')
    reqID = int(fff.readline()) + 1
    fff.close()
    fff = open("RreqID.txt", 'w')
    fff.write(str(reqID))
    fff.close()
    # print("\n\n---------------------------lol----------------------------\n\n")
    # data from `post` request
    data = request.get_json()

    body = {}
    body[reqID] = data

    # publish the data onto the `readQ` queue
    channel.basic_publish(exchange='', routing_key='readQ', body=json.dumps(body))

    
    counter = threading.Thread(target=ReadIsToSlave, args=())
    counter.start()
    

    # getting response from the `responseQ` queue

    # dequeque from `responseQ` until response is got 
    print(reqID)
    global responseDict
    while (reqID not in list(responseDict.keys())):
        fff = open("www.txt", 'w')
        fff.write(json.dumps(responseDict))
        fff.close()

    res = responseDict[reqID]
    responseDict.pop(reqID)
    # print(res)
    # print("\n\n---------------------------lol----------------------------\n\n")

    return jsonify(json.loads(res["result"])), res["status"]




@app.route("/api/v1/worker/list", methods=['GET'])
def list_workers():
    slaves = getOnlyInt(zk.get_children("/slave"))
    master = getOnlyInt(zk.get_children("/master"))
    res = sorted(slaves+master)
    if(len(res)==0):
        return jsonify([]),204
    return jsonify(res),200




@app.route("/api/v1/crash/slave", methods=['GET'])
def crash_slave():
    # zk.stop()
    # zk.start()
    global globalLock
    while (globalLock == False):
        pass
    globalLock = False
    try:
        children = getOnlyInt(zk.get_children("/slave"))
        if(len(children)<=1):
            children = getOnlyInt(zk.get_children("/slave", watch=slavePatrols))
        time.sleep(0.3)
        children = sorted(children)
        pid = children[0]
        cont_id = zk.get("/slave/"+str(pid))[0].decode()
        slave = client.containers.get(cont_id)
        slave.exec_run(["kill","-QUIT","1"])
        while(slave.attrs['State']['Status'] == 'running'):
            slave = client.containers.get(cont_id)
        globalLock = True
    except:
        globalLock = True
        time.sleep(3)
        rr =  requests.get(url = 'http://0.0.0.0:8080/api/v1/crash/slave')
        return rr.json(),rr.status_code
    return jsonify({'pid':int(pid)}),200


@app.route("/api/v1/crash/master", methods=['GET'])
def crash_master():
    # zk.stop()
    # zk.start()
    global globalLock
    while (globalLock == False):
        pass
    globalLock = False
    try:
        children = getOnlyInt(zk.get_children("/master", watch=masterElection))
        children = sorted(children)
        pid = children[0]
        cont_id = zk.get("/master/"+str(pid))[0].decode()
        master = client.containers.get(cont_id)
        master.exec_run(["kill","1"])
        while(master.attrs['State']['Status'] == 'running'):
            master = client.containers.get(cont_id)
        globalLock = True
    except:
        globalLock = True
        time.sleep(3)
        rr = requests.get(url = 'http://0.0.0.0:8080/api/v1/crash/master')
        return rr.json(),rr.status_code

    return jsonify({'pid':int(pid)}),200




def init_sm():
    zk.delete("/slave", recursive=True)
    zk.delete("/master", recursive=True)
    time.sleep(1.5)
    zk.ensure_path("/slave")
    
    zk.ensure_path("/master")
    slavePatrols(1)
    time.sleep(3)
    masterElection(1)
    print("\n\n\n\nAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH\n\n\n\n")
    dscale = threading.Thread(target=downScale, args=())
    dscale.start()
    resp = threading.Thread(target=getResponse, args=())
    resp.start()
    respW = threading.Thread(target=getResponseW, args=())
    respW.start()
    return True
    
init_sm()
ttt = open("timer.txt", "w")
fff = open("counter.txt", "w")
fff.write(str(0))
ttt.write(str(time.time()))
ttt.close()
fff.close()
fff = open("RreqID.txt", 'w')
fff.write(str(0))
fff.close()
fff = open("WreqID.txt", 'w')
fff.write(str(0))
fff.close()

# if __name__ == '__main__':
    
#     init_sm()
#     while(1):
#         pass
#     #app.run(debug=True, host='0.0.0.0', port=1214)