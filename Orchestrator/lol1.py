
import pika
import json
import time
connection = pika.BlockingConnection(
    pika.ConnectionParameters(host='localhost'))
channel = connection.channel()

channel.queue_declare(queue='hello')


body = {1:'a', 2:'b'}
bb = json.dumps(body)

while(1):
    channel.basic_publish(exchange='', routing_key='hello', body=bb)
    print(" [x] Sent 'Hello World!'")
    time.sleep(1)
connection.close()